package com.rostifar.gamecontrol;

/**
 * Created by Dad on 11/13/2015.
 */
public class ScrabbleGameException extends Exception {

    public ScrabbleGameException(String message) {
        super(message);
    }
}
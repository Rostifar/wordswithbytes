package com.rostifar.scrabbleproject;

/**
 * Created by Dad on 11/20/2015.
 */
public class UserInputException extends Exception {
    public UserInputException(String msg) {
        super(msg);
    }
}
